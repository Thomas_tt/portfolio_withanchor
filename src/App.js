import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Navbar from "./Components/Navbar";
import Section from "./Components/Section";
import dummyText from "./DummyText";
import About from './Components/Home';
import Education from './Components/Education';
import Landing from './Components/Landing'
import Social from './Components/Social'
import Skills from './Components/Skills'
import Project from './Components/Project'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <Section
          title=""
          subtitle={<Landing/>}
          dark={true}
          id="section1"
          />
        <Section
          title="About me"
          subtitle={<About/>}
          dark={false}
          id="section2"
        />
        <Section
          title="Education"
          subtitle={<Education/>}
          dark={true}
          id="section3"
        />
        <Section
          title="Skills"
          subtitle={<Skills/>}
          dark={false}
          id="section4"
        />
        <Section
          title="Project"
          subtitle={<Project/>}
          dark={true}
          id="section5"
        />
      </div>
    );
  }
}

export default App;
