import React, { Component } from 'react';
import Social from './Social';
import yt from './yt.png'
import linkedin from './linkedin.png'
import git from './git.png'

class Landing extends Component {
    render() {
        return (
            <div className='landing'>
                <h1>Hello, I'm Thomas !</h1>
                <a target="_blank" rel="noopener noreferrer" href='https://www.youtube.com/channel/UCjlEJVQOQbhJgDajr0UkKRA?view_as=subscriber'>
                <img src={yt} alt='Youtube Channel'style={{alignSelf: 'center'}}/> </a>

                <a target="_blank" rel="noopener noreferrer" href='https://www.linkedin.com/in/thomas-terosiet-5537b212b/'>
                <img src={linkedin} alt='Linkedin'/>
                </a>
                <img src={git} alt='git'/>
                <Social />
            </div>
        )
    }
}

export default Landing