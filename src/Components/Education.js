import React, { Component } from 'react';
import Widecard from './Widecard';

class Education extends Component {
render() {
return (
<div className="condiv">
<Widecard title="Stage developpeur fullstack" where="Yesdoc" from="Juillet 2020" to="Decembre 2020"/>
<Widecard title="EPITECH PGE" where="Epitech Nancy" from="2019" to="2024"/>
<Widecard title="Bac Es" where="Lycee Louise Michel" from="2015" to="2016"/>
</div>
)
}
}
export default Education