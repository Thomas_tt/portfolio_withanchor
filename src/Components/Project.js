import React, {Component} from 'react';
import Link from 'react-scroll/modules/components/Link';

class Project extends Component{
    render() {
        return (
            <div>
                <h3>Projet realise en C</h3>
                <li>Jeu de bataille Navale avec utilisation des signaux sur terminal linux</li>
                <li>Jeu de matchstick contre une IA sur terminal linux</li>
                <li>RPG realise en CSFML</li>
                <br/>
                <h3>Projet realise en C# sur le moteur de jeu Unity</h3>
                <li>Plateformer mario-like</li>
                <li>Escape King: jeu realise lors d'une game Jam de 48 heures</li>
                <br/>
                <h3>Projet JS + React </h3>
                <li>TodoList en JS</li>
                <li>Portfolio en react</li>
                <li>React + firebase: inscription, connexion</li>
            </div>
        )
    }
}

export default Project