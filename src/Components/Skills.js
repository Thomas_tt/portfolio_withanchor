import React, {Component} from 'react';
import {CircularProgressbar, buildStyles} from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

const percent = 60;
const jspercent = 40;
const unitypercent = 50;
class Skills extends Component {
    render (){
        return (
            <div className='wrapp' style={{ width: 100, height: 450}} >
                <div>
                <CircularProgressbar className='C'
                value={percent}
                text={'C'}
                styles = {buildStyles({
                pathColor: 'grey',
                textColor: 'grey' })} />
                </div>
                <br/>
                <div>
                <CircularProgressbar className= 'JS'
                value={jspercent}
                text={'JS'}
                styles = {buildStyles({
                    pathColor: 'grey',
                    textColor: 'grey' })} />
                </div>
                <br/>
                <div>
                <CircularProgressbar className= 'JS'
                value={unitypercent}
                text={'Unity'}
                styles = {buildStyles({
                    pathColor: 'grey',
                    textColor: 'grey' })} />
                </div>
                <br/>
                <div>
                <CircularProgressbar className= 'JS'
                value={30}
                text={'React'}
                styles = {buildStyles({
                    pathColor: 'grey',
                    textColor: 'grey' })} />
                </div>
            </div>
        )
    }
}

export default Skills